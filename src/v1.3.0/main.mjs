export async function setup(ctx) {

	await ctx.loadStylesheet('src/v1.3.0/style.css');
	await ctx.loadTemplates('src/v1.3.0/templates.html');

	ctx.onCharacterLoaded(ctx => {
		const OLD_EQUIPMENT_SLOTS = ["Helmet", "Platebody", "Platelegs", "Boots", "Weapon", "Shield", "Amulet", "Ring", "Gloves", "Quiver", "Cape", "Passive", "Summon1", "Summon2", "Consumable", "Gem"];

		let MICSR_FOUND = (mod.manager.getLoadedModList().find(name => name.indexOf("Combat Simulator") !== -1) != null);

		const COMBAT_ACTION = game.pages.getObjectByID('melvorD:Combat').action;
		const CURRENCY_GP = game.currencies.getObjectByID('melvorD:GP');

		let htmlBlock = null;
		let htmlList = null;
		let storageInfo = { "used": 0 };

		let displayType = 1;
		let reorderMode = false;
		let compactMode = false;

		let presets = null;

		let EDITOR_NODE = null;

		// structures
		const defaultPreset = () => {
			return {
				"title": "New Preset",
				"equipment": [],
				"prayers": [],
				"spells": {},
				"style": {},
				"minibar": [],
				"icon": {
					"type": 0,
					"id": null
				},
				"type": 0,
				"gid": Math.floor(Math.random() * 0xFFFFFFFF)
			};
		};

		const NULL_ITEM = {
			id: null,
			media: cdnMedia('assets/media/main/question.svg'),
			name: 'None',
			sellsFor: {
				currency: CURRENCY_GP,
				quantity: 0
			},
			modifiedDescription: '',
			tier: -1
		};

		const PRESERVE_ITEM = {
			id: 'equipmentPresets:preserve_item',
			media: ctx.getResourceUrl('assets/preserve.png'),
			name: 'Preserve Item',
			sellsFor: {
				currency: CURRENCY_GP,
				quantity: 0
			},
			modifiedDescription: 'Use the item currently placed in the slot.',
			hasDescription: true,
			tier: -1
		};

		let orderMap;
		let iconTypes;
		let attackStyles;
		let spellBooks;

		const initData = () => {
			crcMap = crcCreateMapID();
			orderMap = new Map(game.bank.defaultSortOrder.map((val, i) => [val, i]))
			attackStyles = ['melee', 'ranged', 'magic'];
			iconTypes = [{
				id: 0,
				name: 'None',
				items: [NULL_ITEM]
			},
			{
				id: 1,
				name: 'Skills',
				items: [...game.skills.allObjects]
			},
			{
				id: 2,
				name: 'Items',
				items: game.items.filter(item => item.type != "").sort((a, b) => (orderMap.get(a) || Infinity) - (orderMap.get(b) || Infinity))
			},
			{
				id: 3,
				name: 'Monsters',
				items: game.monsters.filter(monster => monster._name != "")
			},
			{
				id: 4,
				name: 'Dungeons',
				items: [...game.dungeons.allObjects]
			},
			{
				id: 5,
				name: 'Spells',
				items: [...game.attackSpells.allObjects, ...game.curseSpells.allObjects, ...game.auroraSpells.allObjects]
			},
			{
				id: 6,
				name: 'Prayers',
				items: [...game.prayers.allObjects]
			},
			{
				id: 7,
				name: 'Pets',
				items: [...game.pets.allObjects]
			}
			];

			spellBooks = [...game.attackSpellbooks.allObjects.map(spellbook => {
				return {
					id: spellbook.id,
					slot: 'attack',
					tooltip: 'attack-spell-tooltip',
					name: spellbook.name,
					media: spellbook.media,
					spells: spellbook.spells
				}
			}),
			{
				id: 'melvorF:Curses',
				slot: 'curse',
				tooltip: 'curse-spell-tooltip',
				name: getLangString('COMBAT_MISC_CURSE_SPELLBOOK_NAME'),
				media: cdnMedia('assets/media/skills/combat/curses.svg'),
				spells: game.curseSpells.allObjects
			},
			{
				id: 'melvorF:Auroras',
				slot: 'aurora',
				tooltip: 'aurora-spell-tooltip',
				name: getLangString('COMBAT_MISC_AURORA_SPELLBOOK_NAME'),
				media: cdnMedia('assets/media/skills/combat/auroras.svg'),
				spells: game.auroraSpells.allObjects
			}
			]
		}

		// clipboard
		const copyClipboard = (text) => {
			navigator.clipboard.writeText(text).then((e) => {
				notifyPlayer(game.combat, `Copied to clipboard!`, 'success');
			}, (e) => {
				copyClipboardExec(text);
			});
		};

		const copyClipboardExec = (text) => {
			let textArea = createElement("textarea", { attributes: [['style', 'position:fixed;top:0px;left:0px;']] });
			textArea.value = text;

			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();

			try {
				if (document.execCommand('copy')) {
					notifyPlayer(game.combat, `Copied to clipboard!`, 'success');
				}
				else {
					notifyPlayer(game.combat, `Unable to copy to clipboard.`, 'danger');
				}
			} catch (err) {
				notifyPlayer(game.combat, `Unable to copy to clipboard.`, 'danger');
			}

			document.body.removeChild(textArea);
		};

		const deepCopy = (obj) => {
			return JSON.parse(JSON.stringify(obj));
		};

		// config
		const load = () => {
			presets = [];
			const compressedData = ctx.characterStorage.getItem('data');
			storageInfo.used = ctx.characterStorage.size;

			if (compressedData)
				presets = configReader(compressedData);

			displayType = (ctx.characterStorage.getItem('filterType') || 0);
			compactMode = (ctx.characterStorage.getItem('compactMode') || false);
		};

		const save = () => {
			const compressedData = configWriter(presets);
			storage();
			try {
				ctx.characterStorage.setItem('data', compressedData);
				storageInfo.used = ctx.characterStorage.size;
			}
			catch (e) {
				notifyPlayer(game.combat, `Equipment Presets: ${e}`, 'danger');
			}
		};

		// init
		const init = () => {
			// build html elements
			let block = new DocumentFragment();
			block.append(getTemplateNode('preset-manager-template-combat-block'));

			let btnCreate = getAnyElementFromFragment(block, 'create');
			let btnImport = getAnyElementFromFragment(block, 'import');
			let btnExport = getAnyElementFromFragment(block, 'export');
			let btnReorder = getAnyElementFromFragment(block, 'reorder');
			let btnReorderSave = getAnyElementFromFragment(block, 'reorder-save');
			let btnCompact = getAnyElementFromFragment(block, 'compact');
			let btnReset = getAnyElementFromFragment(block, 'reset');

			btnCreate.onclick = (e) => { createPreset(); };
			btnImport.onclick = (e) => { importPreset(); };
			btnExport.onclick = (e) => { exportPresets(); };
			btnReorder.onclick = (e) => { toggleReorder(); };
			btnReorderSave.onclick = (e) => { toggleReorder(); };
			btnCompact.onclick = (e) => { toggleCompact(); };
			btnReset.onclick = (e) => { resetCurrentPrompt(); };

			const filters = ['all', 'combat', 'skill', 'none'];
			filters.forEach((key, index) => {
				let btnFilter = getAnyElementFromFragment(block, `filter-${key}`);
				btnFilter.onclick = (e) => { changeFilter(index); }
			});

			htmlBlock = block.getElementById('preset-manager-container');
			htmlList = block.getElementById('preset-manager-list');

			storageInfo.text = getAnyElementFromFragment(block, 'storage-text');
			storageInfo.bar = getAnyElementFromFragment(block, 'storage-bar');

			build();
			storage();

			if (compactMode)
				htmlList.classList.add("compact");

			$('#combat-fight-container-player').find('.no-gutters').first().append(block);

			// build editor
			EDITOR_NODE = editorBuilder();

			// update save on version change
			if (SAVE_VERSION_UPDATE) {
				SAVE_VERSION_UPDATE = false;
				save();
			}
		};

		const toggleReorder = () => {
			reorderMode = !reorderMode;

			if (reorderMode) {
				htmlList.classList.remove("compact");
				htmlBlock.classList.add("reorder");
				updateListFilter(true);
			}
			else {
				if (compactMode)
					htmlList.classList.add("compact");
	
				htmlBlock.classList.remove("reorder");
				build();
				save();
			}
		};

		const toggleCompact = () => {
			compactMode = !compactMode;

			ctx.characterStorage.setItem('compactMode', compactMode);

			if (compactMode)
				htmlList.classList.add("compact");
			else
				htmlList.classList.remove("compact");
		};

		const changeFilter = (filter) => {
			displayType = filter;
			ctx.characterStorage.setItem('filterType', displayType);
			updateListFilter();
		};

		const updateListFilter = (showAll = false) => {
			showAll ||= displayType == 0;

			presets.forEach((preset) => {
				let node = PRESET_ELM_LIST.get(preset);
				if (preset.type == displayType || (preset.type == 0 && displayType != 3) || showAll) {
					showElement(node);
				} else {
					hideElement(node);
				}
			});
		};

		const PRESET_ELM_LIST = new Map();
		const build = () => {
			PRESET_ELM_LIST.clear();
			htmlList.innerHTML = '';
			presets.forEach((preset, index) => {
				let node = presetHTML(preset, index);
				htmlList.append(node);
				PRESET_ELM_LIST.set(preset, node);
			});
			updateListFilter();
			buildMinibar();
		};

		const storage = () => {
			const sizeLimit = ctx.characterStorage.capacity;
			let percent = ((storageInfo.used / sizeLimit) * 100).toFixed(1);
			storageInfo.text.textContent = `${storageInfo.used} / ${sizeLimit} (${percent}%)`;
			storageInfo.bar.style.width = `${percent}%`;

			if (percent >= 90)
				storageInfo.bar.classList.add('danger');
			else
				storageInfo.bar.classList.remove('danger');
		};

		const presetHTML = (preset, index) => {
			let node = new DocumentFragment();
			node.append(getTemplateNode('preset-manager-template-combat-list'));

			let imgType = getAnyElementFromFragment(node, 'type-image');
			let imgTypeCompact = getAnyElementFromFragment(node, 'type-image-compact');
			let textName = getAnyElementFromFragment(node, 'name');
			let btnEquip = getAnyElementFromFragment(node, 'equip');
			let btnView = getAnyElementFromFragment(node, 'view');
			let btnMoveUp = getAnyElementFromFragment(node, 'up');
			let btnMoveDown = getAnyElementFromFragment(node, 'down');
			let btnCopy = getAnyElementFromFragment(node, 'copy');
			let btnCopyGID = getAnyElementFromFragment(node, 'copy-gid');
			let btnEdit = getAnyElementFromFragment(node, 'edit');
			let btnMICSR = getAnyElementFromFragment(node, 'micsr');
			let btnDelete = getAnyElementFromFragment(node, 'delete');

			let btnCompact = getAnyElementFromFragment(node, 'compact-button');

			// Standard
			textName.innerHTML = preset.title;
			imgType.src = imgTypeCompact.src = presetIcon(preset).media;

			btnCopyGID.innerHTML = `Copy GID: <kbd>0x${preset.gid.toString(16).padStart(8, '0').toUpperCase()}</kbd>`;

			if (!nativeManager.isMobile) {
				tippy(btnView, {
					content: buildTooltip(index),
					allowHTML: true,
					placement: 'right',
					interactive: false,
					animation: false,
					popperOptions: {
						modifiers: [{
							name: 'flip',
							options: { fallbackPlacements: ['left'] }
						}]
					}
				});
			}
			btnView.onclick = (e) => {
				SwalLocale.fire({
					title: preset.title,
					html: buildTooltip(index, false, true),
				});
			};

			btnEquip.onclick = (e) => { equipPreset(index); };
			btnCopy.onclick = (e) => { exportPreset(index); };
			btnEdit.onclick = (e) => { editPreset(index); };
			btnMICSR.onclick = (e) => { micsrPreset(index); };
			btnCopyGID.onclick = (e) => { copyClipboard(`0x${preset.gid.toString(16).padStart(8, '0').toUpperCase()}`); };
			btnDelete.onclick = (e) => { deletePreset(index); };

			btnMoveUp.onclick = (e) => { shiftPreset(-1, preset); };
			btnMoveDown.onclick = (e) => { shiftPreset(1, preset); };

			// Compact
			if (!nativeManager.isMobile) {
				tippy(btnCompact, {
					content: preset.title,
					allowHTML: true,
					placement: 'top',
					interactive: false,
					animation: false
				});
			}

			tippy(btnCompact, {
				content: presetHTMLCompact(preset, index),
				placement: 'bottom',
				interactive: true,
				animation: false,
				trigger: 'click',
				popperOptions: {
					modifiers: [
						{
							name: 'flip',
							options: {
								fallbackPlacements: ['bottom'],
							},
						},
						{
							name: 'preventOverflow',
							options: {
								altAxis: true,
								tether: false,
							},
						},
					],
				},
			});
			btnCompact.ondblclick = (e) => { equipPreset(index); };
			btnCompact.classList.add(`type-${preset.type}`);

			if (MICSR_FOUND)
				showElement(btnMICSR);

			return getAnyElementFromFragment(node, 'main');
		};

		const presetHTMLCompact = (preset, index) => {
			let node = new DocumentFragment();
			node.append(getTemplateNode('preset-manager-template-compact-menu'));

			let btnEquip = getAnyElementFromFragment(node, 'equip');
			let btnCopy = getAnyElementFromFragment(node, 'copy');
			let btnCopyGID = getAnyElementFromFragment(node, 'copy-gid');
			let btnEdit = getAnyElementFromFragment(node, 'edit');
			let btnMICSR = getAnyElementFromFragment(node, 'micsr');
			let btnDelete = getAnyElementFromFragment(node, 'delete');

			btnEquip.onclick = (e) => { equipPreset(index); };
			btnCopy.onclick = (e) => { exportPreset(index); };
			btnEdit.onclick = (e) => { editPreset(index); };
			btnMICSR.onclick = (e) => { micsrPreset(index); };
			btnCopyGID.onclick = (e) => { copyClipboard(`0x${preset.gid.toString(16).padStart(8, '0').toUpperCase()}`); };
			btnDelete.onclick = (e) => { deletePreset(index); };

			if (MICSR_FOUND)
				showElement(btnMICSR);

			return getAnyElementFromFragment(node, 'main');
		}

		const presetAttackType = (index) => {
			let weaponSlot = presets[index].equipment.find(val => val[1] == "melvorD:Weapon");

			if (weaponSlot) {
				let item = game.items.getObjectByID(weaponSlot[0]);
				return item ? item.attackType : null;
			}

			return null;
		};

		const presetIcon = (preset) => {
			if (preset.icon == null || preset.icon.type == null || preset.icon.id == null || preset.icon.type <= 0)
				return NULL_ITEM;

			let group = iconTypes.find(type => type.id == preset.icon.type);
			let icon = group ? group.items.find(item => item.id == preset.icon.id) : NULL_ITEM;

			return icon ? icon : NULL_ITEM;
		};

		const buildTooltip = (index, displayTitle = false, displayMissing = false) => {
			let style = presetAttackType(index);
			let itemList = '';
			let preset = presets[index];

			let item;

			if (displayTitle) {
				itemList += `<span class="text-warning font-w600">${preset.title || ""}</span>`;
				itemList += `<hr class="m-2 border-summoning">`;
			}

			if (preset.equipment != null) {
				preset.equipment.forEach(val => {
					if (val[0] == PRESERVE_ITEM.id) {
						const slot = game.equipmentSlots.getObjectByID(val[1]);
						itemList += `<img src="${slot.emptyMedia}" class="skill-icon-xxs mr-1">${PRESERVE_ITEM.name}: ${slot.emptyName}<br>`;
					}
					else if (item = game.items.getObjectByID(val[0])) {
						itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}${getTooltipQuantity(item, displayMissing)}<br>`;
					}
				});
			}

			if (preset.potion != null) {
				if (item = game.items.getObjectByID(preset.potion)) {
					itemList += `<hr class="m-2 border-summoning">`;
					itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}${getTooltipQuantity(item, displayMissing)}<br>`;
				}
			}

			if (preset.style != null) {
				if (item = game.attackStyles.getObjectByID(preset.style)) {
					itemList += `<hr class="m-2 border-summoning">`;
					itemList += `${item.experienceGain.map(slot => `<img src="${slot.skill.media}" class="skill-icon-xxs mr-1">`).join('')}${item.name}<br>`;
				}
			}

			if (preset.prayers != null) {
				if (preset.prayers.length > 0)
					itemList += `<hr class="m-2 border-summoning">`;
				preset.prayers.forEach(val => {
					if (item = game.prayers.getObjectByID(val)) {
						itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}<br>`;
					}
				});
			}

			if (preset.spells != null) {
				if (style == "magic") {
					itemList += `<hr class="m-2 border-summoning">`;

					if (item = game.attackSpells.getObjectByID(preset.spells.attack))
						itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}<br>`;
					if (item = game.curseSpells.getObjectByID(preset.spells.curse))
						itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}<br>`;
					if (item = game.auroraSpells.getObjectByID(preset.spells.aurora))
						itemList += `<img src="${item.media}" class="skill-icon-xxs mr-1">${item.name}<br>`;
				}
			}

			if (itemList.length == 0)
				return `<span class="text-info">This preset left blank intentionally.</span>`;

			return `<div class="text-center">${itemList}</div>`;
		};

		const getTooltipQuantity = (item, display = true) => {
			if (!display)
				return "";

			const player = game.combat.player;

			let qty = game.bank.getQty(item);

			player.equipment.equippedArray.forEach(val => {
				if (item == val.item) {
					qty += val.quantity;
				}
			});

			if (qty <= 0)
				return ` <small class="text-danger">[Missing]</small>`;

			return "";
		};

		const resetCurrentPrompt = () => {
			Swal.fire({
				title: 'Reset Loadout',
				text: "This will clear your current gear, selected spells and prayers.",
				icon: 'warning',
				showCancelButton: true,
			}).then((result) => {
				if (result.isConfirmed) {
					resetCurrent();
				}
			});
		};

		const resetCurrent = (newPreset = null) => {
			const player = game.combat.player;

			// Unequip Items
			player.equipment.equippedArray.forEach(val => {
				if (newPreset?.equipment.find(item => item[1] == val.slot.id && item[0] == PRESERVE_ITEM.id))
					return;

				if (val.item != val.emptyItem) {
					player.unequipItem(player.selectedEquipmentSet, val.slot);
				}
			});

			// Toggle Spells
			if (player.spellSelection.curse != undefined) player.toggleCurse(player.spellSelection.curse);
			if (player.spellSelection.aurora != undefined) player.toggleAurora(player.spellSelection.aurora);
			game.combat.player.resetAttackSpell();

			// Toggle Prayers
			player.activePrayers.forEach(val => {
				player.togglePrayer(val);
			});
		}

		const importPreset = () => {
			Swal.fire({
				title: 'Import Code',
				input: 'text',
				inputValue: '',
				inputAttributes: {
					autocapitalize: 'off'
				},
				showCancelButton: true,
				confirmButtonText: 'Import'
			}).then((result) => {
				if (result.isConfirmed) {
					try {
						let loadedPreset = JSON.parse(result.value);

						if (Array.isArray(loadedPreset) && loadedPreset.length > 1) {
							importPresetMultipleSWAL(loadedPreset);
							return;
						}

						// Keep GID Unless existing preset matches it.
						if (loadedPreset.gid != undefined && presets.find(fgid => fgid.gid == loadedPreset.gid) != null)
							delete loadedPreset.gid;

						presets.push(Object.assign(defaultPreset(), loadedPreset));
						build();
						save();
					}
					catch (e) {
						notifyPlayer(game.combat, `Unable to import preset.`, 'danger');
						console.error(e);
					}
				}
			});
		};

		const importPresetMultipleSWAL = (importPresets) => {
			const selectedPresets = [];

			const onPresetToggle = (preset) => {
				let idx = selectedPresets.indexOf(preset);

				if (idx >= 0)
					selectedPresets.splice(idx, 1);
				else
					selectedPresets.push(preset);
			};

			let container = createElement('div', { className: 'text-left overflow-hidden text-nowrap' });
			importPresets.forEach((preset, index) => {
				let node = new DocumentFragment();
				node.append(getTemplateNode('preset-manager-template-import-list'));

				let inpInput = getAnyElementFromFragment(node, 'input');
				let inpLabel = getAnyElementFromFragment(node, 'label');
				let textName = getAnyElementFromFragment(node, 'name');
				let textIcon = getAnyElementFromFragment(node, 'icon');

				textName.innerHTML = preset.title;
				textIcon.src = presetIcon(preset).media;
				inpInput.id = `preset-manager-import-id-${index}`;
				inpLabel.setAttribute('for', `preset-manager-import-id-${index}`);
				inpInput.onclick = function (e) {
					onPresetToggle(preset);
				}

				container.append(node);
			});

			SwalLocale.fire({
				title: `Import Presets Select`,
				width: '48em',
				showCancelButton: true,
				showConfirmButton: true,
				html: container,
			}).then((result) => {
				if (result.isConfirmed) {
					try {
						selectedPresets.forEach((loadedPreset) => {
							// Keep GID Unless existing preset matches it.
							if (loadedPreset.gid != undefined && presets.find(fgid => fgid.gid == loadedPreset.gid) != null)
								delete loadedPreset.gid;

							presets.push(Object.assign(defaultPreset(), loadedPreset));
						});

						build();
						save();
					}
					catch (e) {
						notifyPlayer(game.combat, `Unable to import presets.`, 'danger');
						console.error(e);
					}
				}
			});
		};

		const exportPresets = () => {
			copyClipboard(JSON.stringify(presets));
		};

		const exportPreset = (index) => {
			copyClipboard(JSON.stringify(presets[index]));
		};

		const createPreset = () => {
			showEditor(-1).then((result) => {
				if (result.isConfirmed) {
					presets.push(EDITOR_NODE.preset());
					build();
					save();
				}
			});
		};

		const getPreset = (index) => {
			if (index < 0 || index >= presets.length)
				return null;

			return presets[index];
		};

		const addPreset = (preset) => {
			presets.push(Object.assign(defaultPreset(), preset));
			build();
			save();
		}

		const editPreset = (index) => {
			if (index < 0 || index >= presets.length)
				return;

			showEditor(index).then((result) => {
				if (result.isConfirmed) {
					presets[index] = EDITOR_NODE.preset();
					build();
					save();
				}
			});
		};

		const equipPreset = (index) => {
			if (index < 0 || index >= presets.length)
				return;

			equipPresetObject(presets[index]);
		};

		const equipPresetObject = (preset) => {
			if (preset == undefined)
				return;

			resetCurrent(preset);

			const player = game.combat.player;
			let item;
			let slot;

			// Equipment
			if (preset.equipment != null) {
				preset.equipment.forEach(val => {
					slot = game.equipmentSlots.getObjectByID(val[1]);
					item = game.items.getObjectByID(val[0]);
					if (slot && item) {
						if (game.bank.hasItem(item)) {
							player.equipItem(item, player.selectedEquipmentSet, slot, 1000000);
						} else {
							notifyPlayer(game.combat, `Unable to find ${item.name} in bank.`, 'danger');
						}
					}
				});
			}

			// Attack Style
			if (preset.style != null) {
				if (item = game.attackStyles.getObjectByID(preset.style)) {
					player.setAttackStyle(item.attackType, item);
				}
			}

			// Prayers
			if (preset.prayers != null) {
				preset.prayers.forEach(val => {
					if (item = game.prayers.getObjectByID(val)) {
						if (game.prayer.level >= item.level && (item.abyssalLevel === 0 || game.prayer.abyssalLevel >= item.abyssalLevel)) {
							player.togglePrayer(item);
						}
						else {
							notifyPlayer(game.prayer, `Missing level requirements for ${item.name}.`, 'danger');
						}
					}
				});
			}

			// Spells
			if (preset.spells != null) {
				if (item = game.attackSpells.getObjectByID(preset.spells.attack))
					player.selectAttackSpell(item);

				if (item = game.curseSpells.getObjectByID(preset.spells.curse))
					player.toggleCurse(item);

				if (item = game.auroraSpells.getObjectByID(preset.spells.aurora))
					player.toggleAurora(item);
			}

			// Potion
			if (preset.potion != null) {
				if (item = game.items.getObjectByID(preset.potion)) {
					let combatPotion = game.potions.activePotions.get(COMBAT_ACTION);
					let needNewPotion = !combatPotion || combatPotion.item.id != item.id;

					if (needNewPotion) {
						game.potions.usePotion(item);
					}
				}
			}
		};

		const deletePreset = (index) => {
			if (index < 0 || index >= presets.length)
				return;

			presets.splice(index, 1);
			build();
			save();
		};

		const buildPreset = () => {
			let preset = defaultPreset();
			const player = game.combat.player;

			const getSpell = (spell) => spell != undefined ? spell.id : null;

			// Equipment
			preset.equipment = game.combat.player.equipment.equippedArray
				.filter(playerSlot => playerSlot.item != playerSlot.emptyItem && playerSlot.occupiedBy == undefined)
				.map(playerSlot => [playerSlot.item.id, playerSlot.slot.id]);

			// Prayer
			player.activePrayers.forEach(val => {
				preset.prayers.push(val.id);
			});

			// Spells
			preset.spells.attack = getSpell(player.spellSelection.attack);
			preset.spells.curse = getSpell(player.spellSelection.curse);
			preset.spells.aurora = getSpell(player.spellSelection.aurora);

			// Attack Styles
			preset.style = player.attackStyle.id;

			// Combat Potion
			let combatPotion = game.potions.activePotions.get(COMBAT_ACTION);
			if (combatPotion) {
				preset.potion = combatPotion.item.id;
			}
			return preset;
		};

		const buildPresetMICSR = () => {
			const preset = defaultPreset();

			if (typeof mcs == "undefined")
				return preset;

			const settings = mod.api.mythCombatSimulator.export();

			const getSpell = (spell) => spell != undefined && spell != "" ? spell : undefined;

			// Get Weapon Type
			const weaponID = settings.equipment.get("melvorD:Weapon");
			const weapon = game.items.getObjectByID(weaponID);

			// Blocked Equipment Slots
			const blockedSlots = [];
			settings.equipment.forEach((itemID, slotID) => {
				const item = game.items.getObjectByID(itemID);
				if (item && item.occupiesSlots.length > 0) {
					item.occupiesSlots.forEach(slot => {
						if (slot.id == slotID) {
							blockedSlots.push(slot.id);
						}
					});
				}
			});

			// Equipment
			preset.equipment = [...settings.equipment].filter(item => blockedSlots.indexOf(item[0]) == -1).map(item => [item[1], item[0]]);;

			// Prayer
			settings.prayerSelected.forEach(prayerID => {
				preset.prayers.push(prayerID);
			})

			// Spells
			preset.spells.attack = getSpell(settings.spells.attack);
			preset.spells.curse = getSpell(settings.spells.curse);
			preset.spells.aurora = getSpell(settings.spells.aurora);

			// Attack Styles
			if(weapon)
				preset.style = settings.styles[weapon.attackType];

			// Combat Potion
			if (settings.potionID)
				preset.potion = settings.potionID;

			return preset;
		};

		const micsrPreset = (index) => {
			if (typeof mcs == "undefined")
				return;

			const settings = mod.api.mythCombatSimulator.export();
			const preset = presets[index];

			// Equipment
			settings.equipment.clear();
			preset.equipment.forEach(equip => {
				settings.equipment.set(equip[1], equip[0]);
			});

			// Attack Style
			const attackStyle = game.attackStyles.getObjectByID(preset.style);
			if (attackStyle) {
				settings.styles[attackStyle.attackType] = attackStyle.id;
			}

			// Other
			Object.assign(settings.spells, preset.spells);
			settings.potionID = preset.potion;
			settings.prayerSelected = preset.prayers;

			mod.api.mythCombatSimulator.import(settings);
		}

		const shiftPreset = (direction, opreset) => {
			let oidx = presets.indexOf(opreset);
			let nidx = Math.max(0, Math.min(presets.length - 1, oidx + direction));

			if (oidx == -1 || oidx == nidx)
				return;

			// Swap Array
			let npreset = presets[nidx];
			presets[nidx] = opreset;
			presets[oidx] = npreset;

			// Swap HTML
			let node1 = PRESET_ELM_LIST.get(opreset);
			let node2 = PRESET_ELM_LIST.get(npreset);

			if (direction == -1)
				htmlList.insertBefore(node1, node2);
			else
				htmlList.insertBefore(node2, node1);
		};

		const showEditor = (index) => {
			let preset = index >= 0 ? presets[index] : defaultPreset();
			EDITOR_NODE.update(preset);

			return SwalLocale.fire({
				title: `Preset Editor`,
				width: '48em',
				confirmButtonText: 'Save',
				showCancelButton: true,
				showConfirmButton: true,
				allowOutsideClick: false,
				allowEscapeKey: false,
				html: EDITOR_NODE.node,
			});
		};

		// minibar
		let lastMinibarSkill = null;
		const minibarButtons = [];
		const buildMinibar = () => {
			if (lastMinibarSkill == null || !game.settings.showSkillingMinibar) {
				return;
			}

			minibarButtons.forEach((presetItem) => game.minibar.removeItem(presetItem));

			const minibarElement = game.minibar.minibarElement;

			presets.forEach((preset, index) => {
				if (preset.minibar.indexOf(lastMinibarSkill.id) != -1) {
					const presetButton = game.minibar.createMinibarItem(
						`minibar-preset-equipment-${index}`,
						presetIcon(preset).media,
						buildTooltip(index, true),
						{
							onClick: () => equipPreset(index),
						});
					minibarButtons.push(presetButton);
					minibarElement.prepend(presetButton.element);
				}
			});
		};

		// api
		const apiDefaultPreset = () => defaultPreset();
		const apiAddPreset = (preset) => addPreset(preset);
		const apiGetPresets = () => deepCopy(presets);
		const apiGetPresetByIndex = (index) => deepCopy(getPreset(index));
		const apiGetPresetByGID = (gid) => deepCopy(getPreset(presets.indexOf(presets.find(preset => preset.gid == gid))));
		const apiEquipPresetByIndex = (index) => equipPreset(index);
		const apiEquipPresetByGID = (gid) => equipPreset(presets.indexOf(presets.find(preset => preset.gid == gid)));
		const apiRemovePresetByIndex = (index) => deletePreset(index);
		const apiRemovePresetByGID = (gid) => deletePreset(presets.indexOf(presets.find(preset => preset.gid == gid)));
		const apiShowCreate = () => createPreset();
		const apiShowEditByIndex = (index) => editPreset(index);
		const apiShowEditByGID = (gid) => editPreset(presets.indexOf(presets.find(preset => preset.gid == gid)));
		const apiGetPresetIcon = (preset) => presetIcon(preset);

		ctx.api({
			getDefaultPreset: apiDefaultPreset,

			getPresets: apiGetPresets,
			addPreset: apiAddPreset,

			getPresetByIndex: apiGetPresetByIndex,
			getPresetByGID: apiGetPresetByGID,
			equipPresetByIndex: apiEquipPresetByIndex,
			equipPresetByGID: apiEquipPresetByGID,
			removePresetByIndex: apiRemovePresetByIndex,
			removePresetByGID: apiRemovePresetByGID,

			uiShowCreate: apiShowCreate,
			uiShowEditByIndex: apiShowEditByIndex,
			uiShowEditByGID: apiShowEditByGID,
			uiGetPresetIcon: apiGetPresetIcon,
		});

		//////////////////////////////////////////////////////////////////////////////////////////

		const editorBuilder = () => {
			let editorPreset = null;
			let bankFilter = true;

			const modalElement = {
				tabs: {},
				menu: {},
				icon: {},
				equipment: {},
				styles: {},
				spells: {},
				prayer: {},
				potion: null,
				minibar: null
			};

			function createModel() {
				const getFragment = (id) => {
					try { return getAnyElementFromFragment(_content, id); } catch (e) { }
					return undefined;
				};

				let _content = new DocumentFragment();
				_content.append(getTemplateNode('preset-manager-template-modal'));

				const menus = ['equipment', 'style', 'magic', 'prayer', 'potion', 'icon', 'settings'];

				modalElement.menu.name = getFragment('name-field');
				modalElement.menu.name_clear = getFragment('name-clear');
				buildTitle();

				// Tabs
				for (let tabs of menus) {
					let tab = getFragment(`menu-tab-${tabs}`);
					modalElement.menu[tabs] = tab;
					tab.onclick = changeTab.bind(tab, tabs);

					modalElement.tabs[tabs] = getFragment(`content-tab-${tabs}`)
				}

				// Equipment
				modalElement.equipment.container = getFragment('equipment-box');
				modalElement.equipment.slots = getFragment('equipment-box-slots');
				modalElement.equipment.editor = getFragment('slot-editor');
				modalElement.equipment.import = getFragment('import-current');
				modalElement.equipment.import_micsr = getFragment('import-micsr');

				// Equipment - Grid
				const gridSize = EquipmentSlot.getGridSize();
				const numCols = gridSize.cols.max - gridSize.cols.min + 1;
				const numRows = gridSize.rows.max - gridSize.rows.min + 1;
				const colOffset = 1 - gridSize.cols.min;
				const rowOffset = 1 - gridSize.rows.min;
				modalElement.equipment.slots.style.gridTemplateColumns = `repeat(${numCols}, auto)`;
				modalElement.equipment.slots.style.gridTemplateRows = `repeat(${numRows}, auto)`;
				game.equipmentSlots.forEach((slot) => {
					const gridSlot = createElement('div', { parent: modalElement.equipment.slots });
					gridSlot.style.gridColumn = `${slot.gridPosition.col + colOffset}`;
					gridSlot.style.gridRow = `${slot.gridPosition.row + rowOffset}`;

					const gridImg = createElement('img', {
						className: 'combat-equip-img border border-2x border-rounded-equip border-combat-outline p-1 pointer-enabled',
						parent: gridSlot
					});
					modalElement.equipment[slot.id] = gridImg;
				});
				buildEquipment();

				// Attack Styles
				for (let key of attackStyles) {
					modalElement.styles[key] = getFragment(`style-${key}`);
				}
				modalElement.styles.current = getFragment(`style-current`);
				buildStyleData();

				// Spells
				modalElement.spells.container = getFragment('spell-selector');
				modalElement.spells.editor = getFragment('spell-editor');
				spellBooks.forEach(spellbook => {
					createElement('div', { className: 'magic-source', children: [createElement('img', { className: 'skill-icon-sm', attributes: [['src', spellbook.media]] })], parent: modalElement.spells.container });
					modalElement.spells[spellbook.id] = createElement('div', { className: 'spell-button', parent: modalElement.spells.container });
				});
				buildSpellData();

				// Prayer
				modalElement.prayer.standard = getFragment('prayers-standard');
				modalElement.prayer.unholy = getFragment('prayers-unholy');
				modalElement.prayer.abyssal = getFragment('prayers-abyssal');
				buildPrayerData();

				// Potions
				modalElement.potion_options = getFragment('potion-options');
				modalElement.potion = getFragment('potions');
				buildPotionData();

				// Icon
				modalElement.icon.types = getFragment('icon-types');
				modalElement.icon.editor = getFragment('icon-editor');
				modalElement.icon.selected = getFragment('icon-selected-header');
				buildIconData();

				// Type
				modalElement.type = getFragment('type-radios');
				buildTypeData();

				// Minibar
				modalElement.minibar = getFragment('minibar-toggles');
				buildMinibarData();

				return _content;
			}

			const tabOpenHandler = {
				equipment: onEquipmentOpen,
				style: onStyleOpen,
				magic: onMagicOpen,
				icon: onIconOpen,
			};

			function changeTab(tab) {
				for (var tabs in modalElement.tabs) {
					hideElement(modalElement.tabs[tabs]);
					modalElement.menu[tabs].classList.remove("active");
				}

				if (tabOpenHandler[tab] != null) {
					tabOpenHandler[tab]();
				}

				showElement(modalElement.tabs[tab]);
				modalElement.menu[tab].classList.add("active");
			}

			function buildTitle() {
				modalElement.menu.name.onkeyup = function () {
					editorPreset.title = modalElement.menu.name.value;
				};
				modalElement.menu.name_clear.onclick = function (e) {
					modalElement.menu.name.value = '';
				}
			}

			function updateTitle() {
				modalElement.menu.name.value = editorPreset.title;
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Equipment Slots //////////////////////////////////

			const EQUIPMENT_LIST_ELM = new Map();

			function onEquipmentOpen() {
				showElement(modalElement.equipment.container);
				modalElement.equipment.editor.innerHTML = '';
				hideElement(modalElement.equipment.editor);
			}

			function getEquipmentSlotImage(slot, item_id = null) {
				let img = game.equipmentSlots.getObjectByID(slot).emptyMedia;
				let unknown = cdnMedia('assets/media/main/question.svg');
				let item = game.items.getObjectByID(item_id);
				if (item_id == PRESERVE_ITEM.id) return PRESERVE_ITEM.media;
				return (item ? item.media : (item_id ? unknown : img));
			}

			function getEquipmentSlotItem(item_id = null) {
				// Preserve Slot
				if (item_id == PRESERVE_ITEM.id)
					return PRESERVE_ITEM;

				// Item
				let item = game.items.getObjectByID(item_id);
				if (item)
					return item;

				// Unknown
				return {
					id: item_id,
					media: cdnMedia('assets/media/main/question.svg'),
					name: 'Unknown Item',
					modifiedDescription: item_id,
					hasDescription: true,
					sellsFor: 0
				};
			}

			function buildEquipment() {
				game.equipmentSlots.forEach((slot) => {
					let slotElm = modalElement.equipment[slot.id];
					slotElm.onclick = showEquipment.bind(slotElm, slot.id);

					tippy(slotElm, {
						animation: false,
						allowHTML: true,
						content: '',
						zIndex: 999999 // SWAL z-index is set to a high number.
					});
				});

				modalElement.equipment.import.onclick = (e) => { onImportCurrent(); }
				modalElement.equipment.import_micsr.onclick = (e) => { onImportMICSR(); }

				if (MICSR_FOUND)
					showElement(modalElement.equipment.import_micsr);
			}

			function updateEquipmentSelection() {
				game.equipmentSlots.forEach((slot) => {
					let slotElm = modalElement.equipment[slot.id];
					slotElm.src = slot.emptyMedia;
					slotElm._tippy.disable();
				});
				editorPreset.equipment.forEach(slot => {
					let slotElm = modalElement.equipment[slot[1]];
					slotElm.src = getEquipmentSlotImage(slot[1], slot[0]);
					slotElm._tippy.setContent(createItemInformationTooltip(getEquipmentSlotItem(slot[0])));
					slotElm._tippy.enable();
				})
			}

			function equipmentSelectedItem(slot) {
				for (let i = 0; i < editorPreset.equipment.length; i++) {
					let item = editorPreset.equipment[i];
					if (item[1] == slot) {
						return {
							id: item[0],
							index: i,
							item: item
						};
					}
				}

				return {
					id: null,
					index: -1,
					item: null
				};
			}

			function getEquipmentSelectedItemId(slot) {
				return equipmentSelectedItem(slot).id;
			}

			function showEquipment(slot) {
				let slotSearch = EQUIPMENT_LIST_ELM.get(slot);
				if (slotSearch) {
					slotSearch.clear();
				} else {
					const slotData = game.equipmentSlots.getObjectByID(slot);
					const itemList = game.items.equipment.filter(item => item.validSlots.indexOf(slotData) !== -1 && !item.golbinRaidExclusive).sort((a, b) => (orderMap.get(a) || Infinity) - (orderMap.get(b) || Infinity));
					const slotInfo = game.equipmentSlots.getObjectByID(slot);
					itemList.unshift(PRESERVE_ITEM);
					itemList.unshift({
						id: NULL_ITEM.id,
						media: slotInfo.emptyMedia,
						name: 'Nothing',
						modifiedDescription: 'Empty None Nothing',
						sellsFor: {
							currency: CURRENCY_GP,
							quantity: 0
						}
					});

					slotSearch = buildSearchableSelector(slot, itemList, getEquipmentSelectedItemId, onEquipmentSelection, onEquipmentOpen, true);
					EQUIPMENT_LIST_ELM.set(slot, slotSearch);
				}
				hideElement(modalElement.equipment.container);
				modalElement.equipment.editor.innerHTML = '';
				modalElement.equipment.editor.appendChild(slotSearch.node);
				showElement(modalElement.equipment.editor);
			}

			function setFromPreset(preset) {
				editorPreset.equipment = preset.equipment;
				editorPreset.style = preset.style;
				editorPreset.spells = preset.spells;
				editorPreset.prayers = preset.prayers;
				editorPreset.potion = preset.potion;
			}

			function onEquipmentSelection(slot, item) {
				let slotData = equipmentSelectedItem(slot);
				if (slotData.index != -1) {
					if (item.id == null) {
						editorPreset.equipment.splice(slotData.index, 1);
					} else {
						slotData.item[0] = item.id;
					}
				} else {
					if (item.id != null) {
						editorPreset.equipment.push([item.id, slot]);
					}
				}
				updateEquipmentSelection();

				showElement(modalElement.equipment.container);
				modalElement.equipment.editor.innerHTML = '';
				hideElement(modalElement.equipment.editor);
			}

			function onImportCurrent() {
				setFromPreset(buildPreset());
				updateNode();
			}

			function onImportMICSR() {
				setFromPreset(buildPresetMICSR());
				updateNode();
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Attack Style //////////////////////////////////
			const STYLES_ELM_LIST = new Map();

			function onStyleOpen() {
				let weaponSlot = editorPreset.equipment.find(val => val[1] == "Weapon");
				let weaponStyle = 'Unknown';

				if (weaponSlot) {
					let item = game.items.getObjectByID(weaponSlot[0]);
					if (item instanceof WeaponItem) {
						weaponStyle = item.attackType.charAt(0).toUpperCase() + item.attackType.substring(1);
					}
				}
				modalElement.styles.current.innerHTML = weaponStyle;
			}

			function buildStyleData() {
				game.attackStyles.allObjects.forEach((style) => {
					let selectItem = createElement('div', {
						className: 'spell',
						parent: modalElement.styles[style.attackType],
						children: [...style.experienceGain.map(slot => createElement('img', {
							attributes: [
								['src', slot.skill.media]
							]
						})),
						style.name
						]
					});

					selectItem.onclick = function (e) {
						onStyleSelection(style);
					}

					tippy(selectItem, {
						animation: false,
						allowHTML: true,
						content: style.toolTipContent,
						zIndex: 999999 // SWAL z-index is set to a high number.
					});

					STYLES_ELM_LIST.set(style, selectItem);
				});
			}

			function onStyleSelection(style) {
				if (editorPreset.style != style.id) {
					editorPreset.style = style.id;
				}
				else {
					editorPreset.style = null;
				}
				updateStyleSelection();
			}

			function updateStyleSelection() {
				game.attackStyles.allObjects.forEach((style) => {
					const element = STYLES_ELM_LIST.get(style);
					if (editorPreset.style == style.id) {
						element.classList.add('active');
					} else {
						element.classList.remove('active');
					}
				});
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Magic //////////////////////////////////
			const SPELLBOOK_ELM_LIST = new Map();

			function onMagicOpen() {
				showElement(modalElement.spells.container);
				modalElement.spells.editor.innerHTML = '';
				hideElement(modalElement.spells.editor);
			}

			function buildSpellData() {
				spellBooks.forEach(spellbook => {
					let elm = modalElement.spells[spellbook.id];
					elm.onclick = showSpellbook.bind(elm, spellbook, spellbook.spells);
				});
			}

			function showSpellbook(spellbook, spells) {
				let bookData = SPELLBOOK_ELM_LIST.get(spellbook.id);
				if (!bookData) {
					bookData = buildSpellBookSelector(spellbook, spells);
					SPELLBOOK_ELM_LIST.set(spellbook.id, bookData);
				}
				hideElement(modalElement.spells.container);
				modalElement.spells.editor.innerHTML = '';
				modalElement.spells.editor.appendChild(bookData.node);
				showElement(modalElement.spells.editor);
				bookData.update();
			}

			function buildSpellBookSelector(spellbook, spells) {
				const SPELL_NODES = new Map();

				const node = createElement('div', {
					className: 'spell-box'
				});
				const allSpells = [NULL_ITEM, ...spells];

				allSpells.forEach((spell) => {
					let selectItem = createElement('div', {
						className: 'spell',
						parent: node,
						children: [
							createElement('img', {
								attributes: [
									['src', spell.media]
								]
							}),
							spell.name
						]
					});

					selectItem.onclick = function (e) {
						onSpellSelection(spellbook, spell);
					}

					const tooltip = createElement(spellbook.tooltip);
					if (spell.id != null) tooltip.setSpell(spell);

					tippy(selectItem, {
						animation: false,
						allowHTML: true,
						content: spell.id != null ? tooltip : spell.name,
						zIndex: 999999 // SWAL z-index is set to a high number.
					});

					SPELL_NODES.set(spell, selectItem);
				});

				const update = () => {
					const selectedSpellId = editorPreset.spells[spellbook.slot];
					spells.forEach((spell) => {
						const element = SPELL_NODES.get(spell);
						if (selectedSpellId == spell.id) {
							element.classList.add('active');
						} else {
							element.classList.remove('active');
						}
					});
				}

				return {
					node,
					update
				};
			}

			function onSpellSelection(spellbook, spell) {
				if (spellbook.slot == "attack") {
					const fullBook = spell.spellbook;
					editorPreset.spells.attack = spell.id;

					if (!fullBook.allowAuroras)
						editorPreset.spells.aurora = null;
					if (!fullBook.allowCurses)
						editorPreset.spells.curse = null;
				}
				else {
					if (editorPreset.spells[spellbook.slot] == spell.id)
						editorPreset.spells[spellbook.slot] = null;
					else
						editorPreset.spells[spellbook.slot] = spell.id;
				}

				updateSpellSelection();
				onMagicOpen();
			}

			function updateSpellSelection() {
				spellBooks.forEach(spellbook => {
					let elm = modalElement.spells[spellbook.id];

					let spell = spellbook.spells.find(spell => spell.id == editorPreset.spells[spellbook.slot]);
					if (spell != null) {
						elm.innerHTML = `<img class="skill-icon-xs" src="${spell.media}"><span>${spell.name}</span>`;
					}
					else {
						elm.innerHTML = '---';
					}
				});
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Prayers //////////////////////////////////
			const PRAYER_ELM_LIST = new Map();

			function buildPrayerData() {
				const sortedPrayers = game.prayers.allObjects.sort((a, b) => a.level - b.level);
				sortedPrayers.forEach((prayer) => {
					let container = modalElement.prayer.standard;
					if (prayer.isUnholy)
						container = modalElement.prayer.unholy;
					else if (prayer.isAbyssal)
						container = modalElement.prayer.abyssal;

					let selectItem = createElement('div', {
						className: 'spell',
						parent: container,
						children: [
							createElement('img', {
								attributes: [
									['src', prayer.media]
								]
							}),
							prayer.name
						]
					});

					selectItem.onclick = function (e) {
						onPrayerSelection(prayer);
					}

					const content = createElement('prayer-tooltip');
					customElements.upgrade(content);
					content.setPrayer(prayer);

					tippy(selectItem, {
						animation: false,
						allowHTML: true,
						content: content,
						zIndex: 999999 // SWAL z-index is set to a high number.
					});

					PRAYER_ELM_LIST.set(prayer, selectItem);
				});

				game.prayers.allObjects.forEach((prayer) => {

				});
			}

			function updatePrayerSelection() {
				game.prayers.allObjects.forEach((prayer) => {
					const element = PRAYER_ELM_LIST.get(prayer);
					if (editorPreset.prayers.indexOf(prayer.id) != -1) {
						element.classList.add('active');
					} else {
						element.classList.remove('active');
					}
				});
			}

			function onPrayerSelection(prayer) {
				let idx = editorPreset.prayers.indexOf(prayer.id);
				if (idx != -1) {
					editorPreset.prayers.splice(idx, 1);
				} else {
					if (editorPreset.prayers.length < 2) {
						editorPreset.prayers.push(prayer.id);
					}
				}

				updatePrayerSelection();
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Potions //////////////////////////////////
			const combat_action = game.actions.getObjectByID('melvorD:Combat');
			const potionList = game.items.filter(item => item instanceof PotionItem && item.action === combat_action).sort((a, b) => (orderMap.get(a) || Infinity) - (orderMap.get(b) || Infinity));
			potionList.unshift(NULL_ITEM)

			const activePotionTiers = [false, false, false, true];
			const POTION_ELM_LIST = new Map();

			function buildPotionData() {
				const potionTiers = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];

				let max_tier = 0;

				for (let potion of potionList) {
					if (potion.tier > max_tier) {
						max_tier = potion.tier;
					}
				}

				// Tier Toggles
				for (let i = 0; i <= max_tier; i++) {
					let option = createElement('div', {
						className: 'custom-control custom-switch custom-control m-2',
						parent: modalElement.potion_options
					});

					let input = createElement('input', {
						className: 'custom-control-input',
						attributes: [
							['type', 'checkbox'],
							['name', `preset-equipment-potion-tier-${i}`],
							['id', `preset-equipment-potion-tier-${i}`]
						],
						parent: option
					});

					let label = createElement('label', {
						className: 'font-weight-normal ml-2 custom-control-label pointer-enabled',
						attributes: [
							['for', `preset-equipment-potion-tier-${i}`]
						],
						children: [`Tier ${potionTiers[i]}`],
						parent: option
					});

					input.onclick = function (e) {
						onPotionTier(i);
					}

					if (activePotionTiers[i])
						input.checked = true;
				}

				// Build
				potionList.forEach((potion) => {
					let selectItem = createElement('div', {
						className: 'spell d-none',
						parent: modalElement.potion,
						children: [
							createElement('img', {
								attributes: [
									['src', potion.media]
								]
							}),
							potion.name
						]
					});

					selectItem.onclick = function (e) {
						onPotionSelection(potion);
					}

					tippy(selectItem, {
						animation: false,
						allowHTML: true,
						content: createItemInformationTooltip(potion),
						zIndex: 999999 // SWAL z-index is set to a high number.
					});

					POTION_ELM_LIST.set(potion, selectItem);
				});
			}

			function updatePotionSelection() {
				potionList.forEach(potion => {
					const element = POTION_ELM_LIST.get(potion);

					if (editorPreset.potion == potion.id) {
						element.classList.add('active');
					} else {
						element.classList.remove('active');
					}

					if (activePotionTiers[potion.tier] || potion.tier < 0) {
						showElement(element);
					} else {
						hideElement(element);
					}
				});
			}

			function onPotionTier(tier) {
				activePotionTiers[tier] = !activePotionTiers[tier];
				updatePotionSelection();
			}

			function onPotionSelection(potion) {
				editorPreset.potion = potion.id;
				updatePotionSelection();
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Icons //////////////////////////////////
			const ICON_TYPE_ELM_LIST = new Map();

			function onIconOpen() {
				modalElement.icon.editor.innerHTML = '';
			}

			function buildIconData() {
				for (let type of iconTypes) {
					let btn = createElement('button', {
						className: 'btn btn-outline-success m-1 font-size-sm',
						parent: modalElement.icon.types,
						children: [type.name]
					});

					btn.onclick = function (e) {
						onIconTypeSelection(type);
					}
				}
			}

			function onIconTypeSelection(type) {
				let slotSearch = ICON_TYPE_ELM_LIST.get(type);
				if (slotSearch) {
					slotSearch.clear();
				} else {
					slotSearch = buildSearchableSelector(type, type.items, getIconId, onIconSelection);
					ICON_TYPE_ELM_LIST.set(type, slotSearch);
				}

				modalElement.icon.editor.innerHTML = '';
				modalElement.icon.editor.appendChild(slotSearch.node);
			}

			function onIconSelection(type, item) {
				editorPreset.icon.type = type.id;
				editorPreset.icon.id = item.id;
				updateIconSelection();
			}

			function updateIconSelection() {
				let icon = presetIcon(editorPreset);
				modalElement.icon.selected.src = icon.media;
			}

			function getIconId(type) {
				return editorPreset.icon.id;
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Minibar //////////////////////////////////
			const MINIBAR_ELM_LIST = new Map();

			function buildMinibarData() {
				game.skills.forEach((skill) => {
					if (!skill.hasMinibar)
						return;

					const skillId = skill.id.replace(/:/gi, "-");

					let box = createElement('div', {
						className: 'minibar-option font-size-sm',
						parent: modalElement.minibar
					});
					let control = createElement('div', {
						className: 'custom-control custom-switch mb-2',
						parent: box
					});
					let input = createElement('input', {
						className: 'custom-control-input pointer-enabled',
						id: `preset-equipment-minibar-toggle-${skillId}`,
						parent: control,
						attributes: [
							['type', 'checkbox']
						]
					});
					let label = createElement('label', {
						className: 'custom-control-label pointer-enabled',
						parent: control,
						attributes: [
							['for', `preset-equipment-minibar-toggle-${skillId}`]
						],
						children: [
							createElement('img', {
								className: 'skill-icon-xxs mr-1',
								attributes: [
									['src', skill.media]
								]
							}),
							skill.name
						]
					});

					input.onclick = function (e) {
						onMinibarSelection(skill.id);
					}

					MINIBAR_ELM_LIST.set(skill, input);
				});
			}

			function updateMinibarSelection() {
				game.skills.forEach((skill) => {
					if (!skill.hasMinibar)
						return;

					MINIBAR_ELM_LIST.get(skill).checked = (editorPreset.minibar.indexOf(skill.id) != -1);
				});
			}

			function onMinibarSelection(id) {
				let idx = editorPreset.minibar.indexOf(id);
				if (idx != -1) {
					editorPreset.minibar.splice(idx, 1);
				} else {
					editorPreset.minibar.push(id);
				}
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Type //////////////////////////////////
			const TYPE_ELM_LIST = new Map();
			const TYPES_DATA = ['Any', 'Combat', 'Skill'];

			function buildTypeData() {
				TYPES_DATA.forEach((type, i) => {
					let control = createElement('div', {
						className: 'custom-control custom-radio mb-2',
						parent: modalElement.type
					});
					let input = createElement('input', {
						className: 'custom-control-input pointer-enabled',
						id: `preset-equipment-type-${i}`,
						parent: control,
						attributes: [
							['type', 'radio'],
							['name', 'preset-equipment-type']
						]
					});
					let label = createElement('label', {
						className: 'custom-control-label pointer-enabled',
						parent: control,
						attributes: [
							['for', `preset-equipment-type-${i}`]
						],
						children: [type]
					});

					input.onclick = function (e) {
						onTypeSelection(i);
					}

					TYPE_ELM_LIST.set(i, input);
				});
			}

			function updateTypeSelection() {
				TYPES_DATA.forEach((type, i) => {
					TYPE_ELM_LIST.get(i).checked = editorPreset.type == i;
				});
			}

			function onTypeSelection(index) {
				editorPreset.type = index;
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Util: Search Selector //////////////////////////////////
			function buildSearchableSelector(slot, itemList, selectedItem, onSelect, onBack, displayBankFilter = false) {
				const fuse = new Fuse(itemList, {
					shouldSort: false,
					tokenize: true,
					matchAllTokens: true,
					findAllMatches: true,
					threshold: 0.1,
					minMatchCharLength: 1,
					keys: ['name', 'category', 'type', 'description'],
				});

				const searchElmMap = new Map();
				const searchItems = new Set();
				const equipmentQty = new Map();

				// Search
				const SEARCH_FIELD = createElement('input', {
					className: 'form-control',
					attributes: [
						['type', 'text'],
						['placeholder', 'Search...']
					]
				});
				SEARCH_FIELD.onkeyup = function () {
					updateSearchItems();
				};

				// Bank Toggle
				let BANK_TOGGLE = createElement('input', {
					className: 'custom-control-input',
					attributes: [
						['type', 'checkbox'],
						['name', `preset-equipment-filter-bank-${slot}`],
						['id', `preset-equipment-filter-bank-${slot}`]
					],
				});
				BANK_TOGGLE.checked = bankFilter;

				BANK_TOGGLE.onclick = function (e) {
					bankFilter = !bankFilter;
					updateSearchItems();
				}

				let BANK_OPTION = createElement('div', {
					className: 'custom-control custom-switch custom-control m-2 text-nowrap',
					children: [BANK_TOGGLE,
						createElement('label', {
							className: 'font-weight-normal ml-2 custom-control-label pointer-enabled',
							attributes: [
								['for', `preset-equipment-filter-bank-${slot}`]
							],
							children: [`Owned Items Only`]
						})]
				});

				if (!displayBankFilter)
					hideElement(BANK_OPTION);

				// Item List
				const buildSearchItems = () => {
					const container = createElement('div', {
						className: 'd-flex flex-wrap justify-content-center mt-2'
					});

					itemList.forEach((item) => {
						const selectItem = createElement('div', {
							className: "m-1 p-1 rounded d-none bg-combat-inner-dark pointer-enabled",
							parent: container,
							children: [createElement('img', {
								className: 'resize-32',
								attributes: [
									['src', item.media],
									['alt', item.name]
								],
							})]
						});

						selectItem.onclick = function (e) {
							onSelect(slot, item);
							updateSearchItems();
						}

						tippy(selectItem, {
							animation: false,
							allowHTML: true,
							content: (item instanceof Item ? createItemInformationTooltip(item) : item.name),
							zIndex: 999999 // SWAL z-index is set to a high number.
						});

						searchElmMap.set(item, selectItem);
					});

					return container;
				};

				const EQUIPMENT_SWAL_NODE = createElement('div', {
					children: [createElement('div', {
						className: 'd-flex align-items-center mt-2 preset-search-bank',
						children: [SEARCH_FIELD, BANK_OPTION]
					}),
					buildSearchItems()]
				});

				if (onBack != null) {
					const BACK_BUTTON = createElement('button', {
						className: 'btn btn-primary m-1',
						attributes: [
							['type', 'button']
						],
						children: ['Back']
					});
					BACK_BUTTON.onclick = function (e) {
						onBack();
					}
					const BACK_BUTTON_CTN = createElement('div', {
						className: 'w-100 text-center mt-4',
						children: [BACK_BUTTON],
						parent: EQUIPMENT_SWAL_NODE
					})
				}

				if (itemList.length <= 1) {
					hideElement(SEARCH_FIELD);
				}

				const updateSearchTerms = () => {
					let query = SEARCH_FIELD.value;
					searchItems.clear();

					if (query.length >= 1) {
						fuse.search(query).forEach(result => {
							searchItems.add(result);
						});
					}
				};

				const getBankQty = (item) => {
					return game.bank.getQty(item) + (equipmentQty.get(item) || 0);
				};

				const updateSearchItems = () => {
					updateSearchTerms();

					let showAll = SEARCH_FIELD.value.length <= 0;
					const selectedItemId = selectedItem(slot);

					if (bankFilter) {
						equipmentQty.clear();

						game.combat.player.equipmentSets.forEach(equipSet => {
							equipSet.equipment.equippedArray.forEach(equipSlot => {
								equipmentQty.set(equipSlot.item, ((equipmentQty.get(equipSlot.item) || 0) + equipSlot.quantity));
							});
						});
					}

					itemList.forEach(item => {
						const element = searchElmMap.get(item);
						const isFiltered = (displayBankFilter && bankFilter && getBankQty(item) <= 0 && item.id != null);

						if (element === undefined) {
							return;
						}
						if (item.id == selectedItemId) {
							element.classList.add('preset-equipment-selected');
						} else {
							element.classList.remove('preset-equipment-selected');
						}
						if (((searchItems.has(item) || showAll) && !isFiltered) || (item.id != null && item.id.indexOf("equipmentPresets:") == 0)) {
							showElement(element);
						} else {
							hideElement(element);
						}
					});
				};

				const clear = () => {
					SEARCH_FIELD.value = '';
					BANK_TOGGLE.checked = bankFilter;
					updateSearchItems();
				};

				updateSearchItems();

				return {
					node: EQUIPMENT_SWAL_NODE,
					clear
				}
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			function updateNode(preset) {
				if (preset != null)
					editorPreset = Object.assign(defaultPreset(), deepCopy(preset));

				changeTab('equipment');

				updateTitle();

				updateEquipmentSelection();
				updateStyleSelection();
				updateSpellSelection();
				updatePrayerSelection();
				updatePotionSelection();
				updateIconSelection();
				updateTypeSelection();
				updateMinibarSelection();
			}

			function getPreset() {
				return editorPreset;
			}

			const SWAL_NODE = createElement('div', {
				className: 'text-left',
				children: [
					createModel()
				]
			});

			return {
				node: SWAL_NODE,
				update: updateNode,
				preset: getPreset
			};
		};

		//////////////////////////////////////////////////////////////////////////////////////////
		// handle writing and reading the presets in the characterStorage
		// works by mapping all object ids that could be used into a crc32 value that
		// can be stored by writing just a int instead of a variable length string.
		// then uses fflate to compress it even farther.
		// crc32 could technically produce duplicate entries where multiple ids are the same crc
		// this isn't handled as there's no deterministic way to solve it.
		// if a object id is missing/removed, then the read mapping won't exist
		// so it's not entirely safe with modded items, but it would be the same as if it wasn't
		// set in the first place.

		// crc32
		const makeCRCTable = () => {
			var c;
			var crcTable = [];
			for (var n = 0; n < 256; n++) {
				c = n;
				for (var k = 0; k < 8; k++) {
					c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
				}
				crcTable[n] = c;
			}
			return crcTable;
		};
		const crc32 = (str, crcTable) => {
			if (!str || str.length == 0)
				debugger;

			var crc = 0 ^ (-1);
			for (var i = 0; i < str.length; i++) {
				crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
			}
			return (crc ^ (-1)) >>> 0;
		};
		const crcCreateMapID = () => {
			const OLD_STRINGS = [...OLD_EQUIPMENT_SLOTS];
			let crcStrings = [
				PRESERVE_ITEM,
				...OLD_STRINGS.map(str => { return { id: str } }),
				...game.equipmentSlots.allObjects,
				...game.items.allObjects,
				...game.prayers.allObjects,
				...game.attackSpells.allObjects,
				...game.curseSpells.allObjects,
				...game.auroraSpells.allObjects,
				...game.attackStyles.allObjects,
				...game.skills.allObjects,
				...game.monsters.allObjects,
				...game.dungeons.allObjects,
				...game.pets.allObjects
			].map(item => item.id);

			const items = [...new Set(crcStrings)]; // deduplicate
			const crcFrom = new Map(items.map(item => [crc32(item, crcTable), item]));
			const crcTo = new Map(items.map(item => [item, crc32(item, crcTable)]));

			if (items.length != crcFrom.size || items.length != crcTo.size) {
				console.warn(`[equipment-presets] CRC Array length doesn't match Map sizes, possible duplicate!`);
			}

			return {
				from: crcFrom,
				to: crcTo
			};
		};

		let crcMap;
		const crcTable = makeCRCTable();
		const SAVE_MAGIC = "PLMV";
		const SAVE_VERSION = 4;
		let SAVE_VERSION_UPDATE = false;

		// get string from map
		const readMapping = (crc) => {
			if (crc == 0x0)
				return null;

			const item = crcMap.from.get(crc);
			if (!item) {
				//console.warn(`[equipment-presets] Decoded CRC had no matching item: 0x${crc.toString(16)}`);
				return null;
			}
			return item;
		};

		// reading
		const readPreset = (writer, version) => {
			const set = (key, name, value) => value ? (key[name] = value) : null;

			let preset = defaultPreset();
			let len, i;

			preset.title = writer.getString();

			// Equipment
			len = writer.getUint16();
			for (i = 0; i < len; i++) {
				let slot = readMapping(writer.getUint32());
				let equip = readMapping(writer.getUint32());

				if (OLD_EQUIPMENT_SLOTS.indexOf(slot) >= 0) // Upgrade 1.2.2 - > 1.3 Slots
					slot = "melvorD:" + slot;

				if (slot && equip)
					preset.equipment.push([equip, slot]);
			}

			// Potion
			set(preset, "potion", readMapping(writer.getUint32()));

			// Prayer
			len = writer.getUint16();
			for (i = 0; i < len; i++) {
				let prayer = readMapping(writer.getUint32());
				if (prayer)
					preset.prayers.push(prayer);
			}

			// Spells
			if (version >= 4) {
				set(preset.spells, "attack", readMapping(writer.getUint32()));
				set(preset.spells, "curse", readMapping(writer.getUint32()));
				set(preset.spells, "aurora", readMapping(writer.getUint32()));
			}
			else {
				set(preset.spells, "standard", readMapping(writer.getUint32()));
				set(preset.spells, "curse", readMapping(writer.getUint32()));
				set(preset.spells, "aurora", readMapping(writer.getUint32()));
				set(preset.spells, "ancient", readMapping(writer.getUint32()));
				set(preset.spells, "archaic", readMapping(writer.getUint32()));

				// Upgrade 1.2.2 - > 1.3 Slots
				if (preset.spells.standard != null) { preset.spells.attack = preset.spells.standard; delete preset.spells.standard; }
				if (preset.spells.ancient != null) { preset.spells.attack = preset.spells.ancient; delete preset.spells.ancient; }
				if (preset.spells.archaic != null) { preset.spells.attack = preset.spells.archaic; delete preset.spells.archaic; }
			}

			if (version >= 2) {
				// Icon
				preset.icon.type = writer.getUint16();
				preset.icon.id = readMapping(writer.getUint32());

				// Minibar
				len = writer.getUint16();
				for (i = 0; i < len; i++) {
					let skill = readMapping(writer.getUint32());
					if (skill)
						preset.minibar.push(skill);
				}

				// Type
				preset.type = writer.getUint16();
			}

			if (version >= 3) {
				// GID
				preset.gid = writer.getUint32();

				// Attack Styles
				set(preset, "style", readMapping(writer.getUint32()));
			}

			return preset;
		};

		const configReader = (saveString) => {
			//console.log('[equipment-presets] -----read-----');

			const reader = new SaveWriter('Read', 1);
			try {
				reader.setRawData(fflate.unzlibSync(fflate.strToU8(atob(saveString), true)).buffer);

				let MAGIC = reader.getString();
				if (MAGIC != SAVE_MAGIC) {
					console.warn("[equipment-presets] Invalid Preset Config Magic:", MAGIC.substr(0, 4));
					return [];
				}

				let presets = [];
				let version = reader.getUint16(); // version

				if (version > SAVE_VERSION)
					throw new Error('Save preset version higher then script version.');

				let presetCount = reader.getUint16();
				for (let i = 0; i < presetCount; i++) {
					presets.push(readPreset(reader, version));
				}

				if (version < SAVE_VERSION)
					SAVE_VERSION_UPDATE = true;

				return presets;
			} catch (_a) {
				console.warn("[equipment-presets] Preset Manager Config Error", _a);
			}
			return [];
		};

		// writing
		const writerPreset = (writer, preset) => {
			const writeUint32Key = (key, name) => writer.writeUint32((key[name] ? crcMap.to.get(key[name]) : 0) || 0);
			const writeUint32 = (value) => writer.writeUint32(crcMap.to.get(value) || 0);

			writer.writeString(preset.title || "");

			// Equipment
			writer.writeUint16(preset.equipment.length);
			preset.equipment.forEach(equip => {
				writeUint32(equip[1]);
				writeUint32(equip[0]);
			});

			// Potion
			writeUint32(preset.potion);

			// Prayers
			writer.writeUint16(preset.prayers.length);
			preset.prayers.forEach(prayer => {
				writeUint32(prayer);
			});

			// Spells
			writeUint32Key(preset.spells, "attack");
			writeUint32Key(preset.spells, "curse");
			writeUint32Key(preset.spells, "aurora");

			// Icon
			writer.writeUint16(preset.icon.type);
			writeUint32(preset.icon.id);

			// Minibar
			writer.writeUint16(preset.minibar.length);
			preset.minibar.forEach(skill => {
				writeUint32(skill);
			});

			// Type
			writer.writeUint16(preset.type);

			// GID
			writer.writeUint32(preset.gid);

			// Attack Styles
			writeUint32(preset.style);
		};

		const configWriter = (presets) => {
			//console.log('[equipment-presets] -----write-----');
			let writer = new SaveWriter('Write', 128);

			writer.writeString(SAVE_MAGIC);
			writer.writeUint16(SAVE_VERSION);
			writer.writeUint16(presets.length);
			presets.forEach(preset => {
				writerPreset(writer, preset);
			});

			const rawSaveData = writer.getRawData();
			const compressedData = fflate.strFromU8(fflate.zlibSync(new Uint8Array(rawSaveData)), true);
			const saveString = btoa(compressedData);
			//console.log('[equipment-presets] Save Size:', saveString.length, 'bytes');
			return saveString;
		};

		//////////////////////////////////////////////////////////////////////////////////////////
		// run mod
		initData();
		load();

		ctx.onInterfaceReady(ctx => {
			initData();
			load();
			init();

			ctx.patch(Minibar, 'setSkill').after((res, skill) => {
				lastMinibarSkill = skill;
				buildMinibar();
			});
		});
	});
}